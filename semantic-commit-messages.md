# Semantic Commit Messages (커밋 메시지 정의)

더 나은 프로그래머가 될수 있는 약간의 커밋 메시지 스타일 변경 방법.

Format: `<type>(<scope>): <subject>`

`<scope>` is optional

## Example

```
feat: add hat wobble
^--^  ^------------^
|     |
|     +-> Summary in present tense.
|
+-------> Type: chore, docs, feat, fix, refactor, style, or test.
```

More Examples:

- `feat`: 빌드 스크립트에 관한 것이 아닌, 사용자를 위한 새로운 기능에 관한 경우
- `fix`: 빌드 스크립트에 관한 것이 아닌, 사용자를 위한 버그 수정에 관한 경우
- `docs`: 문서에 대한 변경사항이 있는 경우
- `style`: 형식, 세미콜론 누락 등의 사항에 대해 작업한 경우. 코드 생산성에 관한 변경사항은 대상이 아니다.
- `refactor`: 생산성에 관한 코드 리팩토링의 경우, 예: 변수명 변경
- `test`: 누락된 테스트에 대한 추가, 리팩토링 관련 테스트의 경우. 코드 생산성에 관한 변경사항은 대상이 아니다.
- `chore`: 그외 사항들에 대한 업데이트의 경우. 코드 생산성에 관한 변경사항은 대상이 아니다.

Reference(s):

- https://seesparkbox.com/foundry/semantic_commit_messages
- http://karma-runner.github.io/1.0/dev/git-commit-msg.html
